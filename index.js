const express=require('express');
const app= express();
const path=require('path');

app.set('puerto',3000);

app.set('views',path.join(__dirname,'vistas'))
app.set('views engine','ejs');
app.use('/vistas',express.static(path.join(__dirname,'vistas')));

app.use('/public',express.static(path.join(__dirname,'public')));



const servidor=app.listen(app.get('puerto'),()=>{
    console.log('servidor escuchando en puerto ',app.get('puerto'));
});
app.get('/', ( req, res)=>{
      res.render('index.ejs');
    //res.sendFile(path.join(__dirname,'public/index.html'))
})

const SocketIO=require('socket.io');
const io= SocketIO(servidor);


io.on('connection',(socket)=>
{
console.log('nueva conexión',socket.id);
socket.on('chat:mensaje',(datos)=>{
    
    io.sockets.emit('chat:mensaje',datos);
})
socket.on('chat:escribiendo...',(datos)=>{
    socket.broadcast.emit('chat:escribiendo...',datos);
})
});