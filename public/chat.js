const socket=io()
let mensaje= document.getElementById('mensaje');
let usuario= document.getElementById('usuario');
let btn= document.getElementById('Enviar');
let salida= document.getElementById('salida');
let actions= document.getElementById('actions');


btn.addEventListener('click',function ()
{
  socket.emit('chat:mensaje',{
      mensaje: mensaje.value,
      usuario: usuario.value
  });
  mensaje.value='';
}
)

mensaje.addEventListener('keydown',function(){ 
    socket.emit('chat:escribiendo...', usuario.value);
});
socket.on('chat:mensaje',function(datos){
    actions.innerHTML='';
    salida.innerHTML+=`<p>
    <strong>${datos.usuario}</strong>: ${datos.mensaje}
    </p>`
    
})
socket.on('chat:escribiendo...',function(datos){
    
    actions.innerHTML=`<p> <em>${datos} esta escribiendo...</em></p>`
})